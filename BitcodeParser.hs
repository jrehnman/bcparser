
module BitcodeParser where

import Control.Monad
import Data.Bits (Bits, (.&.), (.|.))
import qualified Data.Bits as Bits
import Data.List (lookup, sortBy)
import Data.Maybe
import Data.Word

import qualified BitString as BS
import BitString (BitString)
import qualified BitParser as BP
import BitParser (BitParser, (<|>))
import Data.ByteString (ByteString)

data Bitcode = Bitcode 
    { bc_magic      :: Word16
    , bc_records    :: [Record]
    } deriving Show

data Record 
    = AbbrevRecord AbbrevData
    | UnabbrevRecord UnabbrevData
    | DefineDataRecord AbbrevDef
    | BlockInfo SubBlockHeader [BlockInfoSegment]
    | SubBlock SubBlockHeader [Record]
    deriving Show

data ParserState = PS 
    { ps_abbrevlen      :: Int
    , ps_abbrevs        :: [AbbrevDef]
    , ps_blockinfo      :: [BlockInfoSegment]
    } deriving Show

data AbbrevDef = AbbrevDef
    { def_code          :: DefNumOp
    , def_ops           :: [DefAbbrevOp]
    } deriving Show

data DefAbbrevOp
    = DefNum DefNumOp
    | DefArray { arr_op :: DefAbbrevOp }
    | DefArrayMarker
    | DefChar6 
    | DefBlob
    deriving (Eq, Show)

data DefNumOp 
    = DefLiteral { lit_value :: Int }
    | DefFixed { fix_width :: Int }
    | DefVBR { vbr_wdith :: Int }
    deriving (Eq, Show)

data AbbrevData = AbbrevData
    { abbr_code         :: Int
    , abbr_ops          :: [AbbrevOp]
    } deriving Show

data AbbrevOp
    = OpValue   Int
    | OpBlob    [Word8]
    deriving Show

data BlockInfoRecord
    = SetBID Int
    | BlockName String
    | SetRecordName (Int, String)
    | DefineBlockRecord AbbrevDef
    deriving Show

data UnabbrevData = UnabbrevData
    { uad_code          :: Int
    , uad_ops           :: [Int]
    } deriving Show

data BlockInfoSegment = BlockInfoSegment
    { bis_blockid       :: Int
    , bis_blockname     :: [String]
    , bis_abbrevs       :: [AbbrevDef]
    , bis_recordnames   :: [(Int, String)]
    } deriving Show

data SubBlockHeader = SubBlockHeader
    { b_blockid         :: Int
    , b_newabbrevlen    :: Int
    , b_blocklen        :: Int
    } deriving Show

type AbbrevLen = Int


{-
 - Parse function
 -}
parseBitcode :: ByteString -> Maybe Bitcode
parseBitcode bs = case BP.parse takeBitcode (BS.pack bs) of
    Left bitcode    -> Just bitcode
    Right _         -> Nothing

{-
 - Structures for reading BitCode structure
 -}
takeBitcode :: BitParser Bitcode
takeBitcode = do
    BP.bits bitcodeMagicWord
    appMagicWord    <- BP.anyBits 16
    (records, _)    <- takeRecords initstate
    return $ Bitcode 
        { bc_magic      = appMagicWord
        , bc_records    = records
        }

bitcodeMagicWord :: (Word16, Int)
bitcodeMagicWord = 0x4243 `oflength` 16

initstate :: ParserState
initstate = PS 
    { ps_abbrevlen  = 2
    , ps_abbrevs    = []
    , ps_blockinfo  = []
    }
    
bcAbbrevEndBlock        = 0::Int
bcAbbrevEnterSubblock   = 1::Int
bcAbbrevDefineAbbrev    = 2::Int
bcAbbrevUnabbrevRecord  = 3::Int

takeEnterSubblock :: AbbrevLen -> BitParser SubBlockHeader
takeEnterSubblock abbrevlen = do
    BP.bits (bcAbbrevEnterSubblock `oflength` abbrevlen)
    blockid         <- takeVarWInt 8
    newabbrevlen    <- takeVarWInt 4
    align 32
    blockwords      <- anyWord32
    return $ SubBlockHeader 
        { b_blockid         = blockid
        , b_newabbrevlen    = newabbrevlen
        , b_blocklen        = fromIntegral blockwords * 4
        }

takeEndBlock :: AbbrevLen -> BitParser BitString
takeEndBlock abbrevlen = do
    BP.bits (bcAbbrevEndBlock `oflength` abbrevlen)
    align 32

takeBlock :: (SubBlockHeader -> BitParser x) -> Int -> BitParser x
takeBlock p abbrevlen = do
    blockheader     <- takeEnterSubblock abbrevlen
    (y, sz) <- BP.parserSize $ do
        x <- p blockheader
        takeEndBlock abbrevlen
        return x
    guard (sz == b_blocklen blockheader * 8)
    return y


{-
 - Functions for reading generic BitCode
 -}
takeRecords :: ParserState -> BitParser ([Record], ParserState)
takeRecords ps = do
    (rs, pss) <- liftM unzip $ scanchain (takeRecord . snd) (undefined, ps)
    return (rs, last pss)

takeRecord :: ParserState -> BitParser (Record, ParserState)
takeRecord ps = 
    takeRecordBlock <|>
    (takeBlockInfo          >>=^ updateState ps) <|>
    (takeDataRecord         >>=^ updateState ps) <|>
    (takeUnabbrevRecord     >>=^ updateState ps) <|>
    (takeDefineDataRecord   >>=^ updateState ps)

    where
        abbrevlen = ps_abbrevlen ps
        takeRecordBlock = takeBlock (takeRecordBlockContent ps) abbrevlen
        takeBlockInfo = takeBlock takeBlockInfoContent abbrevlen
        takeDataRecord = liftM AbbrevRecord (takeAbbrevData ps)
        takeUnabbrevRecord = liftM UnabbrevRecord (takeUnabbrevData abbrevlen)
        takeDefineDataRecord = liftM DefineDataRecord (takeDefineAbbrev abbrevlen)

takeUnabbrevData :: AbbrevLen -> BitParser UnabbrevData
takeUnabbrevData abbrevlen = do
    BP.bits (bcAbbrevUnabbrevRecord `oflength` abbrevlen)
    code    <- takeVarWInt 6
    n_ops   <- takeVarWInt 6
    ops     <- count n_ops (takeVarWInt 6)
    return  $ UnabbrevData { uad_code = code, uad_ops = ops }

takeRecordBlockContent :: ParserState -> SubBlockHeader -> BitParser (Record, ParserState)
takeRecordBlockContent ps blockheader = do
    guard (b_blockid blockheader >= 8) -- Standard blocks need to be specially handled
    (rs, ps')   <- takeRecords (stateFromBlock blockheader ps)
    return      $ (SubBlock blockheader rs, ps { ps_blockinfo = ps_blockinfo ps' })

stateFromBlock :: SubBlockHeader -> ParserState -> ParserState
stateFromBlock hdr ps = ps 
    { ps_abbrevlen  = b_newabbrevlen hdr
    , ps_abbrevs    = lookupBlockAbbrevs ps (b_blockid hdr)
    }

lookupBlockAbbrevs :: ParserState -> Int -> [AbbrevDef]
lookupBlockAbbrevs ps blockid = concat [bis_abbrevs seg | seg <- ps_blockinfo ps, bis_blockid seg == blockid]

updateState :: ParserState -> Record -> (Record, ParserState)
updateState ps r@(DefineDataRecord ops) = (r, ps { ps_abbrevs    = ps_abbrevs ps ++ [ops] })
updateState ps r@(BlockInfo _ segments) = (r, ps { ps_blockinfo  = ps_blockinfo ps ++ segments })
updateState ps r = (r, ps)

{-
 - Functions for reading Info blocks
 -}
takeBlockInfoContent :: SubBlockHeader -> BitParser Record
takeBlockInfoContent blockheader = do
    guard (b_blockid blockheader == 0)
    segments    <- many (takeBlockInfoSegment (b_newabbrevlen blockheader))
    return      $ BlockInfo blockheader segments

takeBlockInfoSegment :: AbbrevLen -> BitParser BlockInfoSegment
takeBlockInfoSegment abbrevlen = do
    bid <- takeSetBid abbrevlen
    rs <- many $
        takeBlockName abbrevlen <|> 
        takeSetRecordName abbrevlen <|> 
        liftM DefineBlockRecord (takeDefineAbbrev abbrevlen)
    return $ BlockInfoSegment 
        { bis_blockid = bid
        , bis_blockname = concatMap extractBlockName rs
        , bis_abbrevs = concatMap extractAbbrevDef rs
        , bis_recordnames = concatMap extractRecordName rs }

    where 
        extractBlockName :: BlockInfoRecord -> [String]
        extractBlockName (BlockName name) = [name]
        extractBlockName _ = []

        extractAbbrevDef :: BlockInfoRecord -> [AbbrevDef]
        extractAbbrevDef (DefineBlockRecord def) = [def]
        extractAbbrevDef _ = []

        extractRecordName :: BlockInfoRecord -> [(Int, String)]
        extractRecordName (SetRecordName recordname) = [recordname]
        extractRecordName _ = []

takeSetBid :: AbbrevLen -> BitParser Int
takeSetBid abbrevlen = do
    d <- takeUnabbrevData abbrevlen
    guard (uad_code d == 1)
    guard (length (uad_ops d) == 1)
    return . head . uad_ops $ d

takeBlockName :: AbbrevLen -> BitParser BlockInfoRecord
takeBlockName abbrevlen = do
    d <- takeUnabbrevData abbrevlen
    guard (uad_code d == 2)
    return . BlockName . bytesToString . uad_ops $ d

takeSetRecordName :: AbbrevLen -> BitParser BlockInfoRecord
takeSetRecordName abbrevlen = do
    d <- takeUnabbrevData abbrevlen
    guard (uad_code d == 3)
    guard (length (uad_ops d) > 1)
    let (recordid:name) = uad_ops d
    return . SetRecordName $ (recordid, bytesToString name)

{-
 - Functions for reading an abbreviated record, based on an anbbreviation definition
 -}
takeAbbrevData :: ParserState -> BitParser AbbrevData
takeAbbrevData ps = do
    recordid <- takeFixedWInt (ps_abbrevlen ps)
    guard (recordIsDefined ps recordid)
    let abbrevdef = ps_abbrevs ps !! (recordid - 4)
    codeop <- takeNumericalOp (def_code abbrevdef)
    dataops <- liftM concat $ mapM takeAbbrevOp (def_ops abbrevdef)
    return $ AbbrevData 
        { abbr_code = codeop
        , abbr_ops  = dataops
        }

    where
        recordIsDefined :: ParserState -> Int -> Bool
        recordIsDefined ps rid = (rid >= 4) && (rid < length (ps_abbrevs ps) + 4)

takeNumericalOp :: DefNumOp -> BitParser Int
takeNumericalOp (DefLiteral n) = return n
takeNumericalOp (DefFixed w)   = takeFixedWInt w
takeNumericalOp (DefVBR w)     = takeVarWInt w

takeAbbrevOp :: DefAbbrevOp -> BitParser [AbbrevOp]
takeAbbrevOp (DefNum opdef) = takeNumericalOp opdef >>= return . return . OpValue
takeAbbrevOp DefChar6       = take6bitChar >>= return . return . OpValue
takeAbbrevOp (DefArray op)  = do
    arraylen <- takeVarWInt 6
    liftM concat $ count arraylen (takeAbbrevOp op)

takeAbbrevOp DefBlob        = do
    bloblen <- takeVarWInt 6
    align 32
    blob <- count bloblen anyByte
    align 32
    return [OpBlob blob]

{-
 - Functions for reading an abbreviation definition
 -}
takeDefineAbbrev :: AbbrevLen -> BitParser AbbrevDef
takeDefineAbbrev abbrevlen = do
    BP.bits (bcAbbrevDefineAbbrev `oflength` abbrevlen)
    numOps <- takeVarWInt 5
    guard (numOps >= 1)
    codeop <- takeDefNumericalOp
    ops <- count (numOps - 1) takeDefAbbrevOp >>= return . collapseDefArray
    guard (not (elem DefArrayMarker ops))
    return $ AbbrevDef
        { def_code = codeop
        , def_ops = ops
        }

    where
        collapseDefArray :: [DefAbbrevOp] -> [DefAbbrevOp]
        collapseDefArray (DefArrayMarker:op:[]) = [DefArray op]
        collapseDefArray (op:ops) = op:(collapseDefArray ops)
        collapseDefArray [] = []

takeDefNumericalOp :: BitParser DefNumOp
takeDefNumericalOp =
    takeDefLiteral <|>
    takeDefFixed <|>
    takeDefVBR
    
takeDefAbbrevOp :: BitParser DefAbbrevOp
takeDefAbbrevOp =
    (liftM DefNum takeDefNumericalOp) <|>
    takeDefArray <|>
    takeDefChar6 <|>
    takeDefBlob
    
takeDefLiteral :: BitParser DefNumOp
takeDefLiteral = do
    BP.bits ((1::Int) `oflength` 1)
    liftM DefLiteral (takeVarWInt 8)

takeDefFixed :: BitParser DefNumOp
takeDefFixed = liftM DefFixed (takeDefEncodedAbbrevWithData 1)

takeDefVBR :: BitParser DefNumOp
takeDefVBR = liftM DefVBR (takeDefEncodedAbbrevWithData 2)

takeDefArray :: BitParser DefAbbrevOp
takeDefArray = takeDefEncodedAbbrev 3 >> return DefArrayMarker

takeDefChar6 :: BitParser DefAbbrevOp
takeDefChar6 = takeDefEncodedAbbrev 4 >> return DefChar6

takeDefBlob :: BitParser DefAbbrevOp
takeDefBlob = takeDefEncodedAbbrev 5 >> return DefBlob

takeDefEncodedAbbrevWithData :: Int -> BitParser Int
takeDefEncodedAbbrevWithData code = takeDefEncodedAbbrev code >> takeVarWInt 5

takeDefEncodedAbbrev :: Int -> BitParser Int
takeDefEncodedAbbrev code = do
    BP.bits ((0::Int) `oflength` 1)
    BP.bits (code `oflength` 3)

{-
 - BitCode primitives
 -}
takeFixedWInt :: Bits a => Int -> BitParser a
takeFixedWInt = BP.anyBits

takeVarWInt :: Bits a => Int -> BitParser a
takeVarWInt w = do
    lschunks <- many (takeNonTerminal w)
    mschunk <- BP.anyBits w
    return $ foldl (BS.concat (w-1)) mschunk lschunks

    where
        takeNonTerminal :: Bits a => Int -> BitParser a
        takeNonTerminal w = do
            chunk <- BP.anyBits (w-1)
            BP.bits ((1::Int) `oflength` 1)
            return chunk

take6bitChar :: BitParser Int
take6bitChar = liftM decode6BitChar (BP.anyBits 6)
       
decode6BitChar :: Int -> Int
decode6BitChar n 
    | n <= 25 = n + fromEnum 'a'
    | n <= 51 = n + fromEnum 'A'
    | n <= 61 = n + fromEnum '0'
decode6BitChar 62 = fromEnum '.'
decode6BitChar 63 = fromEnum '_'

align :: Int -> BitParser BitString
align w = do
    x <- BP.tell
    BP.any $ let x' = (x `mod` w) in if (x' == 0) then 0 else (w - x')

{-
 - BitParser combinators
 -}

-- Syntactiv sugar for use with BP.bits
oflength :: a -> Int -> (a, Int)
oflength = (,)

anyByte :: BitParser Word8
anyByte = BP.anyBits 8

-- Little Endian
anyWord16 :: BitParser Word16
anyWord16 = do
    lsb <- anyByte
    msb <- anyByte
    return $ BS.concat 8 (fromIntegral msb) (fromIntegral lsb)

-- Little Endian
anyWord32 :: BitParser Word32
anyWord32 = do
    lsw <- anyWord16
    msw <- anyWord16
    return $ BS.concat 16 (fromIntegral msw) (fromIntegral lsw)

many1 :: BitParser a -> BitParser [a]
many1 p = do
    a <- p
    as <- many p
    return (a:as)

many :: BitParser a -> BitParser [a]
many p = many1 p <|> return []

count :: Int -> BitParser a -> BitParser [a]
count n p = mapM (const p) (replicate n ())

chain' :: (a -> BitParser a) -> (a -> b -> b) -> a -> b -> BitParser b
chain' p f a0 b0 = (p a0 >>= \a -> chain' p f a b0 >>= \b -> return (f a b)) <|> return b0

chain :: (a -> BitParser a) -> a -> BitParser a
chain p a0 = chain' p (flip const) a0 a0

scanchain :: (a -> BitParser a) -> a -> BitParser [a]
scanchain p a0 = chain' p (:) a0 []

(>>=^) :: Monad m => m a -> (a -> b) -> m b
m >>=^ f = m >>= return . f

bytesToString :: [Int] -> String
bytesToString = map toEnum

