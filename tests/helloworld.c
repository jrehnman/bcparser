
/*
 * LLVM Bitcode     - clang -c -emit-llvm helloworld.c
 * LLVM Assembler   - clang -s -emit-llvm helloworld.c
 */

#include <stdio.h>

int main( void )
{
    printf("Hello, World!\n");
    return 0;
}

