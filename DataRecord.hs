
module DataRecord where

import Control.Applicative
import Control.Monad

import BitcodeParser 


recordCode :: Record -> Maybe Int
recordCode (UnabbrevRecord recorddata) = return (uad_code recorddata)
recordCode (AbbrevRecord recorddata) = return (abbr_code recorddata)
recordCode _ = fail "Not a data record"

recordData :: Record -> Maybe [AbbrevOp]
recordData (UnabbrevRecord recorddata) = return (map OpValue (uad_ops recorddata))
recordData (AbbrevRecord recorddata) = return (abbr_ops recorddata)
recordData _ = fail "Not a data record"

recordDataAt :: Int -> Record -> Maybe AbbrevOp
recordDataAt n r = liftM head (recordDataFrom n r)

recordDataFrom :: Int -> Record -> Maybe [AbbrevOp]
recordDataFrom n r = do
    values <- recordData r
    guard (n < length values)
    return $ drop n values

recordString :: [AbbrevOp] -> Maybe String
recordString = mapM intField >=> return . map toEnum

intField :: AbbrevOp -> Maybe Int
intField (OpValue n) = return n
intField _ = fail "Expected number"

boolField :: AbbrevOp -> Maybe Bool
boolField (OpValue 0) = return False
boolField (OpValue _) = return True
boolField _ = fail "Expected boolean value"

enumField :: Enum e => AbbrevOp -> Maybe e
enumField = intField >=> return . toEnum

expectCode :: Int -> Record -> Maybe Record
expectCode n r = do
    code <- recordCode r
    guard (code == n)
    return r

parseStringRecord :: Record -> Maybe String
parseStringRecord = recordDataFrom 0 >=> recordString

parseIntRecord :: Record -> Maybe Int
parseIntRecord r = do
    ops <- recordData r
    case ops of
        [OpValue n] -> return n
        _           -> fail "Data mismatch"

newtype RecordParser a = RP { runRecord :: Int -> Record -> Maybe (Int, a) }

instance Monad RecordParser where
    return a = RP $ \n _ -> Just (n, a)
    ma >>= f = RP $ \n r -> do
        (n', a) <- runRecord ma n r
        runRecord (f a) n' r

instance Functor RecordParser where
    fmap f fa = fa >>= return . f

instance Applicative RecordParser where
    pure = return
    ff <*> fa = do
        f <- ff
        a <- fa
        return (f a)

rpStart :: Int -> a -> RecordParser a
rpStart code a = RP $ \n r -> expectCode code r >> return (n, a)

-- Parse next field in record
rp :: (AbbrevOp -> Maybe a) -> RecordParser a
rp f = RP $ \n r -> recordDataAt n r >>= f >>= \a -> return (n+1, a)

-- Parse remaining fields in record
rpEnd :: ([AbbrevOp] -> Maybe a) -> RecordParser a
rpEnd f = RP $ \n r -> do
    rdata <- recordDataFrom n r
    a <- f rdata
    return (length rdata, a)

infixr 3 `parsedWith`
parsedWith :: RecordParser a -> Record -> Maybe a
rp `parsedWith` r = runRecord rp 0 r >>= return . snd

