
module LLVM.Records where


data Module = Module [ModuleRecord] deriving Show

data ModuleRecord
    -- SubBlocks
    = BlockInfo
    | ModuleBlockParamAttr      [ParamAttrEntry]
--    | ModuleBlockParamAttrGroup ParamAttrGroupBlock
--    | ModuleBlockConstants      ConstantsBlock
    | ModuleBlockFunction       [FunctionRecord]
--    | ModuleBlockValueSymTab    ValueSymTabBlock
--    | ModuleBlockMetadata       MetadataBlock
    | ModuleBlockType           [TypeRecord]
    -- Records
    | ModuleVersion             Int
    | ModuleTriplet             String
    | ModuleDataLayout          String
    | ModuleAsm                 String
    | ModuleSectionName         String
    | ModuleDepLib              String
    | ModuleGlobalVar           CodeGlobalVar
    | ModuleFunction            CodeFunction
    | ModuleAlias               CodeAlias
    | ModulePurgeVals           Int
    | ModuleGcName              String
    deriving Show

data ParamAttrEntry = ParamAttrEntry
    { paramAttrIdx      :: Int
    , paramAttributes   :: [ParamAttr]
    } deriving Show

data TypeRecord
    = TypeNumEntry              Int
    | TypeVoid
    | TypeFloat
    | TypeDouble
    | TypeLabel
    | TypeOpaque
    | TypeInteger               Int
    | TypePointer               TypeCodePointer
    | TypeFunctionOld           TypeCodeFunctionOld
    | TypeHalf
    | TypeArray                 TypeCodeSequence
    | TypeVector                TypeCodeSequence
    | TypeX86Fp80
    | TypeX86Fp128
    | TypePPCFp128
    | TypeMetadata
    | TypeX86MMX
    | TypeStructAnon            TypeCodeStruct
    | TypeStructName            String
    | TypeStructNamed           TypeCodeStruct
    | TypeFunction              TypeCodeFunction
    deriving Show

data FunctionRecord
    = FuncDeclareBlocks         Int
    | FuncInstBinOp             BinaryOpcode OpType OpVal OpVal
    | FuncInstCast              CastOpcode OpType OpType OpVal
    | FuncInstGep               [OpVal]
    | FuncInstSelect            OpType OpVal OpVal OpVal
    | FuncInstExtractElt        OpType OpVal OpVal
    | FuncInstInsertElt         OpType OpVal OpVal OpVal
    | FuncInstShuffleVec        OpType OpVal OpVal OpVal
    | FuncInstCmp               OpType OpVal OpVal Pred
    | FuncInstRet
    | FuncInstRetValue          OpType OpVal
    | FuncInstBr                BB
    | FuncInstCondBr            BB BB Cond
    | FuncInstSwitch            OpType [OpVal]
    | FuncInstInvoke            FunAttr OpType [OpVal]
    | FuncInstUnreachable
--    | FuncInstPhi
--    | FuncInstAlloca
--    | FuncInstLoad
--    | FuncInstVaArg
--    | FuncInstStore
    | FuncInstExtractVal        [OpVal]
    | FuncInstInsertVal         [OpVal]
    | FuncInstCmp2              OpType OpVal OpVal Pred
    | FuncInstVSelect           OpType OpVal OpVal OpType Pred
    | FuncInstInboundsGep       [OpVal]
    | FuncInstIndirectBr        OpType [OpVal]
    | FuncDebugLocAgain
    | FuncInstCall              FunAttr OpType FunId [OpVal]
--    | FuncDebugLoc
--    | FuncInstFence
--    | FuncInstCmpXchg
--    | FuncInstAtomicRMW
    | FuncInstResume            OpVal
--    | FuncInstLandingpad
--    | FuncInstLoadAtomic
--    | FuncInstStoreAtomic
    deriving Show

type OpVal = Int
type OpType = Int
type Pred = Int
type BB = Int
type Cond = Int
type FunAttr = Int
type FunId = Int

data CodeGlobalVar = CodeGlobalVar
    { gv_pointerType    :: Int -- Type index
    , gv_isConst        :: Bool
    , gv_initid         :: Int
    , gv_linkage        :: Linkage
    , gv_alignment      :: Int
    , gv_section        :: Int -- Section index
    , gv_visibility     :: Maybe Visibility
    , gv_threadLocal    :: Maybe ThreadLocalStorage
    , gv_unnamedAddr    :: Maybe Bool
    } deriving Show

data CodeFunction = CodeFunction
    { fn_functionType   :: Int -- Type index
    , fn_callingConv    :: CallingConv
    , fn_isProto        :: Bool
    , fn_linkage        :: Linkage
    , fn_paramAttr      :: Int -- ParamAttr index
    , fn_alignment      :: Int
    , fn_section        :: Int -- Section index
    , fn_visibility     :: Visibility
    , fn_gc             :: Maybe Int -- GcName index
    , fn_unnamedAddr    :: Maybe Bool
    } deriving Show

data CodeAlias = CodeAlias
    { alias_type        :: Int -- Type index
    , alias_aliasee     :: Int -- Value index?
    , alias_linkage     :: Linkage
    , alias_visibility  :: Maybe Visibility
    } deriving Show

data TypeCodePointer = TypeCodePointer
    { pt_pointee        :: Int -- Type index
    , pt_addressSpace   :: Maybe Int
    } deriving Show

data TypeCodeFunction = TypeCodeFunction
    { ft_vararg         :: Bool
    , ft_returnType     :: Int -- Type index
    , ft_paramTypes     :: [Int] -- Type indices
    } deriving Show

data TypeCodeFunctionOld = TypeCodeFunctionOld
    { fto_vararg        :: Bool
    , fto_attrId        :: Int
    , fto_returnType    :: Int
    , fto_paramTypes    :: [Int]
    } deriving Show

data TypeCodeStruct = TypeCodeStruct
    { st_isPacked       :: Bool
    , st_elTypes        :: [Int] -- Type indices
    } deriving Show

data TypeCodeSequence = TypeCodeSequence
    { at_numElems       :: Int
    , at_elType         :: Int -- Type index
    } deriving Show

data Linkage
    = External
    | Weak
    | Appending
    | Internal
    | LinkOnce
    | DllImport
    | DllExport
    | Extern_Weak
    | Common
    | Private
    | Weak_Odr
    | LinkOnce_Odr
    | Available_Externally
    | Linker_Private
    deriving (Enum, Show)

data Visibility
    = DefaultVisibility
    | HiddenVisibility
    | ProtectedVisibility
    deriving (Enum, Show)

data ThreadLocalStorage
    = NotThreadLocal
    | ThreadLocal
    | LocalDynamic
    | InitialExec
    | LocalExec
    deriving (Enum, Show)

data CallingConv
    = CCC
    | FastCC
    | ColdCC
    | X86StdCallCC
    | X86FastCallCC
    | ArmApcsCC
    | ArmAapcsCC
    | ArmAapcsVfpCC
    deriving Show

data ParamAttr
    = NoAttr
    | ZeroExt
    | SignExt
    | NoReturn
    | InReg
    | StructRet
    | NoUnwind
    | NoAlias
    | ByVal
    | Nest
    | ReadNone
    | ReadOnly
    | NoInline
    | AlwaysInline
    | OptSize
    | StackProtect
    | StackProtectReq
    | Align Int
    | NoCapture
    | NoRedZone
    | NoImplicitFloat
    | Naked
    | InlineHint
    | AlignStack Int
    | ReturnsTwice
    | UWTable
    | NonLazyBind
    | AddressSafety
    | MinSize
    | NoDuplicate
    | StackProtectStrong
    | ThreadSafety
    | UninitializedChecks
    deriving (Eq, Show)

data CastOpcode
    = Truncate
    | ZeroExtend
    | SignExtend
    | FloatToUInt
    | FloatToSInt
    | UIntToFloat
    | SIntToFloat
    | TruncateFloat
    | ExtendFloat
    | PointerToInt
    | IntToPointer
    | Bitcast
    deriving (Enum, Show)

data BinaryOpcode
    = BinOpAdd
    | BinOpSub
    | BinOpMul
    | BinOpUDiv
    | BinOpSDiv -- Overloaded for FP
    | BinOpURem 
    | BinOpSRem -- Overloaded for FP
    | BinOpShl
    | BinOpLShr
    | BinOpAShr
    | BinOpAnd
    | BinOpOr
    | BinOpXor
    deriving (Enum, Show)

