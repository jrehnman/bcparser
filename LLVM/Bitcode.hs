
module LLVM.Bitcode where

import Control.Applicative
import Control.Monad
import Data.Bits
import Data.Maybe

import BitcodeParser hiding (BlockInfo)
import DataRecord
import qualified BitcodeParser as Bitcode
import LLVM.Records


parseLLVM :: Bitcode -> Maybe Module
parseLLVM (Bitcode 0xC0DE [modulerecord]) = parseModule modulerecord
parseLLVM _ = fail "Invalid Bitcode"

parseModule :: Record -> Maybe Module
parseModule moduleblock@(SubBlock hdr records) = do
    subBlockId 8 moduleblock
    liftM Module (mapM parseModuleRecord records)

parseModuleRecord :: Record -> Maybe ModuleRecord
parseModuleRecord = oneOf
    -- Blocks
    [ parseBlockInfo                            >=> return . const BlockInfo
    , subBlockId 9  >=> paramAttrBlock          >=> return . ModuleBlockParamAttr
    --, subBlockId 10 >=> paramAttrGroupBlock     >=> return . ModuleBlockParamAttrGroup
    --, subBlockId 11 >=> constantsBlock          >=> return . ModuleBlockConstants
    , subBlockId 12 >=> functionBlock           >=> return . ModuleBlockFunction
    -- 13 unused
    --, subBlockId 14 >=> valueSymTabBlock        >=> return . ModuleBlockValueSymTab
    --, subBlockId 15 >=> metadataBlock           >=> return . ModuleBlockMetadata
    -- 16, Metadata Attachment, not allowed at module level
    , subBlockId 17 >=> typesBlock              >=> return . ModuleBlockType
    --, subBlockId 13 >=> typeSymTabBlock         >=> return . ModuleBlockTypeSymTab -- Gone MIA
    -- Records
    , expectCode 1  >=> parseIntRecord          >=> return . ModuleVersion
    , expectCode 2  >=> parseStringRecord       >=> return . ModuleTriplet
    , expectCode 3  >=> parseStringRecord       >=> return . ModuleDataLayout
    , expectCode 4  >=> parseStringRecord       >=> return . ModuleAsm
    , expectCode 5  >=> parseStringRecord       >=> return . ModuleSectionName
    , expectCode 6  >=> parseStringRecord       >=> return . ModuleDepLib
    , expectCode 7  >=> parseCodeGlobalVar      >=> return . ModuleGlobalVar
    , expectCode 8  >=> parseCodeFunction       >=> return . ModuleFunction
    , expectCode 9  >=> parseCodeAlias          >=> return . ModuleAlias
    , expectCode 10 >=> parseIntRecord          >=> return . ModulePurgeVals
    , expectCode 11 >=> parseStringRecord       >=> return . ModuleGcName
    ]

parseBlockInfo :: Record -> Maybe ()
parseBlockInfo (Bitcode.BlockInfo _ _) = return ()
parseBlockInfo _ = fail "Not a BlockInfo record"

paramAttrBlock :: [Record] -> Maybe [ParamAttrEntry]
paramAttrBlock = mapM parseAttrRecord >=> return . concat

parseAttrRecord :: Record -> Maybe [ParamAttrEntry]
parseAttrRecord = oneOf
    [ expectCode 1 >=> parseParamAttrEntryOld
    --, expectCode 2 >=> parseParamAttrEntry
    ]

typesBlock :: [Record] -> Maybe [TypeRecord]
typesBlock = mapM parseTypeRecord

parseTypeRecord :: Record -> Maybe TypeRecord
parseTypeRecord = oneOf
    [ expectCode 1  >=> parseIntRecord          >=> return . TypeNumEntry
    , expectCode 2                              >=> return . const TypeVoid
    , expectCode 3                              >=> return . const TypeFloat
    , expectCode 4                              >=> return . const TypeDouble
    , expectCode 5                              >=> return . const TypeLabel
    , expectCode 6                              >=> return . const TypeOpaque
    , expectCode 7  >=> parseIntRecord          >=> return . TypeInteger
    , expectCode 8  >=> parsePointerType        >=> return . TypePointer
    , expectCode 9  >=> parseFunctionTypeOld    >=> return . TypeFunctionOld
    , expectCode 10                             >=> return . const TypeHalf
    , expectCode 11 >=> parseSequenceType       >=> return . TypeArray
    , expectCode 12 >=> parseSequenceType       >=> return . TypeVector
    , expectCode 13                             >=> return . const TypeX86Fp80
    , expectCode 14                             >=> return . const TypeX86Fp128
    , expectCode 15                             >=> return . const TypePPCFp128
    , expectCode 16                             >=> return . const TypeMetadata
    , expectCode 17                             >=> return . const TypeX86MMX
    , expectCode 18 >=> parseStructType         >=> return . TypeStructAnon
    , expectCode 19 >=> parseStringRecord       >=> return . TypeStructName
    , expectCode 20 >=> parseStructType         >=> return . TypeStructNamed
    , expectCode 21 >=> parseFunctionType       >=> return . TypeFunction
    ]

functionBlock :: [Record] -> Maybe [FunctionRecord]
functionBlock = mapM parseFunctionRecord

parseFunctionRecord :: Record -> Maybe FunctionRecord
parseFunctionRecord = oneOf
    [ declareBlocks
    , instBinaryOperator
    , instCast
    , instGep
    , instSelect
    , instExtractElement
    , instInsertElement
    , instShuffleVector
    , instCompare
    , instReturn
    , instBranch
    , instSwitch
    , instInvoke
    , instUnreachable
    , instExtractValue
    , instInsertValue
    , instCompare2
    , instVSelect
    , instInboundsGep
    , instIndirectBranch
    , debugLocAgain
    , instCall
    , instResume
    ]

{-
 - Function-block records
 -}
declareBlocks :: Record -> Maybe FunctionRecord
declareBlocks = parsedWith $ rpStart 1
    FuncDeclareBlocks <*> rp intField

instBinaryOperator :: Record -> Maybe FunctionRecord
instBinaryOperator = parsedWith $ rpStart 2
    FuncInstBinOp <*> rp enumField <*> rp intField <*> rp intField <*> rp intField

instCast :: Record -> Maybe FunctionRecord
instCast = parsedWith $ rpStart 3
    FuncInstCast <*> rp enumField <*> rp intField <*> rp intField <*> rp intField

instGep :: Record -> Maybe FunctionRecord
instGep = parsedWith $ rpStart 4
    FuncInstGep <*> rpEnd (mapM intField)

instSelect :: Record -> Maybe FunctionRecord
instSelect = parsedWith $ rpStart 5
    FuncInstSelect <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instExtractElement :: Record -> Maybe FunctionRecord
instExtractElement = parsedWith $ rpStart 6
    FuncInstExtractElt <*> rp intField <*> rp intField <*> rp intField

instInsertElement :: Record -> Maybe FunctionRecord
instInsertElement = parsedWith $ rpStart 7
    FuncInstInsertElt <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instShuffleVector :: Record -> Maybe FunctionRecord
instShuffleVector = parsedWith $ rpStart 8
    FuncInstShuffleVec <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instCompare :: Record -> Maybe FunctionRecord
instCompare = parsedWith $ rpStart 9
    FuncInstCmp <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instReturn :: Record -> Maybe FunctionRecord
instReturn r =
    (rpStart 10 FuncInstRetValue <*> rp intField <*> rp intField `parsedWith` r) `mplus`
    (rpStart 10 FuncInstRet `parsedWith` r)

instBranch :: Record -> Maybe FunctionRecord
instBranch r =
    (rpStart 11 FuncInstCondBr  <*> rp intField <*> rp intField <*> rp intField `parsedWith` r) `mplus`
    (rpStart 11 FuncInstBr      <*> rp intField `parsedWith` r)

instSwitch :: Record -> Maybe FunctionRecord
instSwitch = parsedWith $ rpStart 12
    FuncInstSwitch <*> rp intField <*> rpEnd (mapM intField)

instInvoke :: Record -> Maybe FunctionRecord
instInvoke = parsedWith $ rpStart 13
    FuncInstInvoke <*> rp intField <*> rp intField <*> rpEnd (mapM intField)

instUnreachable :: Record -> Maybe FunctionRecord
instUnreachable r = expectCode 15 r >> return
    FuncInstUnreachable

instExtractValue :: Record -> Maybe FunctionRecord
instExtractValue = parsedWith $ rpStart 26
    FuncInstExtractVal <*> rpEnd (mapM intField)

instInsertValue :: Record -> Maybe FunctionRecord
instInsertValue = parsedWith $ rpStart 27
    FuncInstInsertVal <*> rpEnd (mapM intField)

instCompare2 :: Record -> Maybe FunctionRecord
instCompare2 = parsedWith $ rpStart 28
    FuncInstCmp2 <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instVSelect :: Record -> Maybe FunctionRecord
instVSelect = parsedWith $ rpStart 29
    FuncInstVSelect <*> rp intField <*> rp intField <*> rp intField <*> rp intField <*> rp intField

instInboundsGep :: Record -> Maybe FunctionRecord
instInboundsGep = parsedWith $ rpStart 30
    FuncInstInboundsGep <*> rpEnd (mapM intField)

instIndirectBranch :: Record -> Maybe FunctionRecord
instIndirectBranch = parsedWith $ rpStart 31
    FuncInstIndirectBr <*> rp intField <*> rpEnd (mapM intField)

debugLocAgain :: Record -> Maybe FunctionRecord
debugLocAgain r = expectCode 33 r >> return
    FuncDebugLocAgain

instCall :: Record -> Maybe FunctionRecord
instCall = parsedWith $ rpStart 34
    FuncInstCall <*> rp intField <*> rp intField <*> rp intField <*> rpEnd (mapM intField)

instResume :: Record -> Maybe FunctionRecord
instResume = parsedWith $ rpStart 39
    FuncInstResume <*> rp intField

{-
 - Module-block records
 -}
parseCodeGlobalVar :: Record -> Maybe CodeGlobalVar
parseCodeGlobalVar r = pure CodeGlobalVar <*>
    (recordDataAt 0 r >>= intField) <*>         -- Pointer type index
    (recordDataAt 1 r >>= boolField) <*>        -- Is const
    (recordDataAt 2 r >>= intField) <*>         -- Init ID
    (recordDataAt 3 r >>= enumField) <*>        -- Linkage
    (recordDataAt 4 r >>= intField) <*>         -- Alignment
    (recordDataAt 5 r >>= intField) <*>         -- Section index
    pure (recordDataAt 6 r >>= enumField) <*>   -- Visibility (optional)
    pure (recordDataAt 7 r >>= enumField) <*>   -- Thread local storage (optional)
    pure (recordDataAt 8 r >>= boolField)       -- Unnamed address (optional)

parseCodeFunction :: Record -> Maybe CodeFunction
parseCodeFunction r = pure CodeFunction <*>
    (recordDataAt 0 r >>= intField) <*>         -- Function type index
    (recordDataAt 1 r >>= parseCallingConv) <*> -- Calling convention
    (recordDataAt 2 r >>= boolField) <*>        -- Is prototype
    (recordDataAt 3 r >>= enumField) <*>        -- Linkage
    (recordDataAt 4 r >>= intField) <*>         -- Param attribute index
    (recordDataAt 5 r >>= intField) <*>         -- Alignment
    (recordDataAt 6 r >>= intField) <*>         -- Section index
    (recordDataAt 7 r >>= enumField) <*>        -- Visibility
    pure (recordDataAt 9 r >>= intField) <*>    -- Garbage collector index (optional)
    pure (recordDataAt 10 r >>= boolField)      -- Unnamed address (optional)

parseCodeAlias :: Record -> Maybe CodeAlias
parseCodeAlias r = pure CodeAlias <*>
    (recordDataAt 0 r >>= intField) <*>         -- Type index
    (recordDataAt 1 r >>= intField) <*>         -- Value index
    (recordDataAt 2 r >>= enumField) <*>        -- Linkage
    pure (recordDataAt 3 r >>= enumField)       -- Visibility (optional)

parseCallingConv :: AbbrevOp -> Maybe CallingConv
parseCallingConv op = do
    n <- intField op
    lookup n
        [ (0,   CCC)
        , (8,   FastCC)
        , (9,   ColdCC)
        , (64,  X86StdCallCC)
        , (65,  X86FastCallCC)
        , (66,  ArmApcsCC)
        , (67,  ArmAapcsCC)
        , (68,  ArmAapcsVfpCC)
        ]

{-
 - ParamAttr-block records
 -}

parseParamAttrEntryOld :: Record -> Maybe [ParamAttrEntry]
parseParamAttrEntryOld r = do
    values <- recordDataFrom 0 r >>= mapM intField
    guard (even (length values))
    let paramIndices    = [n | (n, idx) <- zip values [0..], even idx]
        attrBitfields   = [bits | (bits, idx) <- zip values [0..], odd idx]
    return $ zipWith ParamAttrEntry
        paramIndices
        (map paramAttrLiterals attrBitfields)

paramAttrLiterals :: Int -> [ParamAttr]
paramAttrLiterals n =
    let align = paramAttrAlign n
        stackAlign = paramAttrStackAlign n
        flags = [ paramAttrLiteral (n .&. mask) | mask <- [1..(2^45)] ]
    in filter (/= NoAttr) (align:stackAlign:flags)

paramAttrLiteral :: Int -> ParamAttr
paramAttrLiteral                0x0 = NoAttr
paramAttrLiteral                0x1 = ZeroExt
paramAttrLiteral                0x2 = SignExt
paramAttrLiteral                0x4 = NoReturn
paramAttrLiteral                0x8 = InReg
paramAttrLiteral               0x10 = StructRet
paramAttrLiteral               0x20 = NoUnwind
paramAttrLiteral               0x40 = NoAlias
paramAttrLiteral               0x80 = ByVal
paramAttrLiteral              0x100 = Nest
paramAttrLiteral              0x200 = ReadNone
paramAttrLiteral              0x400 = ReadOnly
paramAttrLiteral              0x800 = NoInline
paramAttrLiteral             0x1000 = AlwaysInline
paramAttrLiteral             0x2000 = OptSize
paramAttrLiteral             0x4000 = StackProtect
paramAttrLiteral             0x8000 = StackProtectReq
                                   -- 16 bits Alignment (only lower 7 bits used)
paramAttrLiteral        0x100000000 = NoCapture
paramAttrLiteral        0x200000000 = NoRedZone
paramAttrLiteral        0x400000000 = NoImplicitFloat
paramAttrLiteral        0x800000000 = Naked
paramAttrLiteral       0x1000000000 = InlineHint
                                   -- 3 bits Stack alignment
paramAttrLiteral      0x10000000000 = ReturnsTwice
paramAttrLiteral      0x20000000000 = UWTable
paramAttrLiteral      0x40000000000 = NonLazyBind
paramAttrLiteral      0x80000000000 = AddressSafety
paramAttrLiteral     0x100000000000 = MinSize
paramAttrLiteral     0x200000000000 = NoDuplicate
paramAttrLiteral     0x400000000000 = StackProtectStrong
paramAttrLiteral     0x800000000000 = ThreadSafety
paramAttrLiteral    0x1000000000000 = UninitializedChecks

paramAttrAlign :: Int -> ParamAttr
paramAttrAlign n =
    let alignbits = (n `shiftR` 16) .&. 0x1F
    in if alignbits /= 0
        then Align (2 ^ (alignbits - 1))
        else NoAttr

paramAttrStackAlign :: Int -> ParamAttr
paramAttrStackAlign n =
    let stackalignbits = (n `shiftR` 37) .&. 0x7
    in if stackalignbits /= 0
        then AlignStack (2 ^ (stackalignbits - 1))
        else NoAttr

{-
 - Type-block records
 -}
parsePointerType :: Record -> Maybe TypeCodePointer
parsePointerType r = do
    pointee <- recordDataAt 0 r >>= intField
    return TypeCodePointer
        { pt_pointee = pointee
        , pt_addressSpace = recordDataAt 1 r >>= intField
        }

parseFunctionType :: Record -> Maybe TypeCodeFunction
parseFunctionType r = pure TypeCodeFunction <*>
    (recordDataAt 0 r >>= boolField) <*>        -- Is vararg
    (recordDataAt 1 r >>= intField) <*>         -- Return type index
    (recordDataFrom 2 r >>= mapM intField)      -- Parameter type indices

parseFunctionTypeOld :: Record -> Maybe TypeCodeFunctionOld
parseFunctionTypeOld r = pure TypeCodeFunctionOld <*>
    (recordDataAt 0 r >>= boolField) <*>        -- Is vararg
    (recordDataAt 1 r >>= intField) <*>         -- Attribute Id ?
    (recordDataAt 2 r >>= intField) <*>         -- Return type index
    (recordDataFrom 3 r >>= mapM intField)      -- Parameter type indices

parseStructType :: Record -> Maybe TypeCodeStruct
parseStructType r = do
    isPacked <- recordDataAt 0 r >>= boolField
    elTypes <- recordDataFrom 1 r >>= mapM intField
    return TypeCodeStruct
        { st_isPacked = isPacked
        , st_elTypes = elTypes
        }

parseSequenceType :: Record -> Maybe TypeCodeSequence
parseSequenceType r = do
    numElems <- recordDataAt 0 r >>= intField
    elType <- recordDataAt 1 r >>= intField
    return TypeCodeSequence
        { at_numElems = numElems
        , at_elType = elType
        }

{-
 - Misc.
 -}
subBlockId :: Int -> Record -> Maybe [Record]
subBlockId n (SubBlock (SubBlockHeader id_n _ _) records) = guard (id_n == n) >> return records
subBlockId _ _ = fail "Not a SubBlock"

oneOf :: MonadPlus m => [x -> m y] -> x -> m y
oneOf fs x = (foldl1 mplus . map ($ x)) fs

